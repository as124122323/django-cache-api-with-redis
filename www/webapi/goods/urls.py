from django.urls import path, re_path
from . import views

urlpatterns = [
    path('show_time/', views.ShowCurrentTimeView.as_view(), name='show_time'),
    path('show_time2/', views.ShowCurrentTime2View.as_view(), name='show_time'),
]