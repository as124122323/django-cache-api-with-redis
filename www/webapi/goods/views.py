from django.http import JsonResponse
from rest_framework import views
from datetime import datetime
import logging
from utils import cache_related

logger = logging.getLogger(__name__)

class ShowCurrentTimeView(views.APIView):

    @cache_related.cache_view_page(timeout=60, cache_key_func=cache_related.get_cache_key_with_path_and_update_time)
    def get(self, request, *args, **kwargs):
        logger.info("request.path: %s" % request.path)
        try:
            data = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
        except Exception as e:
            logger.error(e)
            return JsonResponse({
                "status": "failed", 
                "result": None, 
                "error_msg": e
            })
        return JsonResponse({
            "status": "success", 
            "result": data, 
            "error_msg": None
        })


class ShowCurrentTime2View(views.APIView):

    @cache_related.cache_view_page(timeout=None, cache_key_func=cache_related.get_cache_key_with_path_and_update_time)
    def get(self, request, *args, **kwargs):
        logger.info("request.path: %s" % request.path)
        try:
            data = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
        except Exception as e:
            logger.error(e)
            return JsonResponse({
                "status": "failed", 
                "result": None, 
                "error_msg": e
            })
        return JsonResponse({
            "status": "success", 
            "result": data, 
            "error_msg": None
        })