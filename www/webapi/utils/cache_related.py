from django.core.cache import CacheHandler
from django.conf import settings
from functools import wraps
import logging

logger = logging.getLogger(__name__)

# CACHE_MIDDLEWARE_ALIAS 默認為 default 
alias = settings.CACHE_MIDDLEWARE_ALIAS  
caches = CacheHandler()
cache = caches[alias]  # 找到要使用的 cache 設定

def cache_view_page(timeout=None, cache_key_func=None):
    """專門用於 DRF View API 的快取緩存裝飾器。

    模仿 django-redis 自帶的 cache_page() 寫法的快取裝飾器，主要是添加 cache_key_func 參數。
    此裝飾器只能用於 DRF View API 的方法。

    Args:
        timeout: 快取 Key 的過期時間，單位為秒。（若為 None 代表永不過期）
        cache_key_func: 可自訂 Cache Key 的產生方式。（類別為一函數）

    Returns:
        返回 response 對象。
    """
    def _deco(view_func):
        # 使用 @wraps 裝飾器是為了讓 .__doc__ 和 .__name__ 會正確找到被裝飾函數，而不是 _handler()
        @wraps(view_func)
        def _handler(self, request, *args, **kwargs):
            if cache_key_func is None:
                key = request.get_full_path_info()
            else:
                key = cache_key_func(request, *args, **kwargs)
            logger.info("cache key: %s" % key)

            response = cache.get(key)
            if response is None:
                response = view_func(self, request, *args, **kwargs)
                cache.set(key, response, timeout)
            return response

        return _handler

    return _deco


def get_cache_key_with_path_and_update_time(request, *args, **kwargs):
    request_full_path = request.get_full_path_info()
    update_time = "2022-12-16T00:00:00"

    key = "{request_full_path}|{update_time}".format(
        request_full_path=request_full_path, update_time=update_time
    )
    logger.info("get_cache_key_with_path_and_update_time key: %s" % key)
    return key
